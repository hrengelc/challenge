import { IAlbumData } from "interfaces/albumData.interface";

const axios = require("axios");

const trelloBaseUrl = "https://api.trello.com/1";

export default class TrelloHttp {
  static createTrelloCard = (
    idList: string = "61a9202eaffc7f74e4adf189",
    cardItems: IAlbumData[],
    initialCounter: number = 0
  ) => {
    if (initialCounter < cardItems.length) {
      console.log(initialCounter, cardItems.length);
      const createCardUrl = encodeURI(
        `${trelloBaseUrl}/cards` +
          `?key=${process.env.TRELLO_KEY}` +
          `&token=${process.env.TRELLO_TOKEN}` +
          `&idList=${idList}` +
          `&name=Title: ${cardItems[initialCounter].title} - Year: ${cardItems[initialCounter].year}` +
          `&urlSource=${
            cardItems[initialCounter].cover.length > 0
              ? cardItems[initialCounter].cover[0].url
              : ""
          }`
      );
      return axios
        .post(createCardUrl)
        .then(function (response: { data: any }) {
          return TrelloHttp.createTrelloCard(
            idList,
            cardItems,
            (initialCounter += 1)
          );
        })
        .catch((errors: any) => {
          console.log(errors);
          console.log(errors.response.status, errors.response.statusText);
        });
    } else return true;
  };

  static createTrelloList = (
    listName: string,
    idBoard: string = "61a7283c9540c4076fabac9a"
  ) => {
    console.log("createTrelloCard");

    const createCardUrl =
      `${trelloBaseUrl}/lists` +
      `?key=${process.env.TRELLO_KEY}` +
      `&token=${process.env.TRELLO_TOKEN}` +
      `&idBoard=${idBoard}` +
      `&name=${listName}`;

    console.log(createCardUrl);

    return axios.post(createCardUrl);
  };

  // createBulkTrelloCard ===================
  static createBulkTrelloCard = async (cards: string[], idList: string) => {
    let requestsArray: string[] = [];
    cards.forEach((element: any) => {
      const requestURL =
        `${trelloBaseUrl}/cards` +
        `?key=${process.env.TRELLO_KEY}` +
        `&token=${process.env.TRELLO_TOKEN}` +
        `&idList=${idList}` +
        `&name=Album: ${element.title} Year: ${element.year}` +
        `&urlSource=${element.cover.length > 0 ? element.cover[0].url : ""}`;

      requestsArray.push(axios.post(requestURL));
    });

    await axios.all(requestsArray).catch((errors: any) => {
      console.log(errors.response.status, errors.response.statusText);
    });
  };

  // createBulkTrelloList ===================
  static createBulkTrelloList = (
    albumsArray: any,
    idBoard: string = "61a7283c9540c4076fabac9a"
  ) => {
    const albumsTittleArray = [];

    for (const [key, value] of Object.entries(albumsArray)) {
      albumsTittleArray.push(key);
    }

    let requestsArray: string[] = [];
    albumsTittleArray.forEach((element: any) => {
      const requestURL =
        `${trelloBaseUrl}/lists` +
        `?key=${process.env.TRELLO_KEY}` +
        `&token=${process.env.TRELLO_TOKEN}` +
        `&idBoard=${idBoard}` +
        `&name=${element}`;

      requestsArray.push(axios.post(requestURL));
    });

    axios
      .all(requestsArray)
      .then(
        axios.spread(async (...responses: any) => {
          const responsesSize = responses.length;
          let responsesInnerItems = 0;

          while (responsesInnerItems <= responsesSize) {
            let innerDataSize =
              albumsArray[responses[responsesInnerItems].data.name].length;
            console.log("innerDataSize> ", innerDataSize);

            const dataResponse = await this.createTrelloCard(
              responses[responsesInnerItems].data.id,
              albumsArray[responses[responsesInnerItems].data.name]
            ).then();

            responsesInnerItems += 1;
            console.log(dataResponse);
          }
        })
      )
      .catch((errors: any) => {
        console.log(errors.response.status, errors.response.statusText);
      });
  };
}
