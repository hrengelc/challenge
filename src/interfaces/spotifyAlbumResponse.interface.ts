import { ISpotifyAlbumItemInterface } from "./spotifyAlbumItem.interface";

export interface ISpotifyAlbumResponseInterface {
  href: string;
  items: ISpotifyAlbumItemInterface[];
  limit: number;
  next: string;
  offset: number;
  previous: string;
  total: number;
}
