export interface IAlbum {
  year: string;
  title: string;
}
