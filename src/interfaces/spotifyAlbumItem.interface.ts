import { ISpotifyArtistsInterface } from "./spotifyArtists.interface";
import { ISpotifyAlbumImagesInterface } from "./spotifyAlbumImages.interface";

export interface ISpotifyAlbumItemInterface {
  album_group: string;
  album_type: string;
  artists?: ISpotifyArtistsInterface[];
  available_markets?: string[];
  external_urls?: {
    spotify: string;
  };
  href: string;
  id: string;
  images: ISpotifyAlbumImagesInterface[];
  name: string;
  release_date: string;
  release_date_precision: string;
  total_tracks: number;
  type: string;
  uri: string;
}
