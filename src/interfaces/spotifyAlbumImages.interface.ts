export interface ISpotifyAlbumImagesInterface {
  height: number;
  url: string;
  width: number;
}
