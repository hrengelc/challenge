import { ISpotifyAlbumImagesInterface } from "./spotifyAlbumImages.interface";

export interface IAlbumData {
  year: string;
  title: string;
  cover: ISpotifyAlbumImagesInterface[];
  trelloListTitle: string;
}
