import { ISpotifyAlbumResponseInterface } from "interfaces/spotifyAlbumResponse.interface";
import { ISpotifyAlbumImagesInterface } from "interfaces/spotifyAlbumImages.interface";
import Utils from "../../../utils/utils";

const getFirstWordOfStringValidateCases = [
  {
    parameter: "",
    expected: "",
  },
  {
    parameter: "test one two",
    expected: "test",
  },
  {
    parameter: "test",
    expected: "test",
  },
];

describe("Get first word of a string.", () => {
  test.each(
    getFirstWordOfStringValidateCases.map((testCase) => [
      testCase.parameter,
      testCase.expected,
    ])
  )(
    `Test %#: Received the parameter: ${JSON.stringify(
      "%j"
    )}, expected corrected validation of ${JSON.stringify("%j")}.`,
    (parameter, expected) => {
      expect(expected).toStrictEqual(Utils.getFirstWord(parameter));
    }
  );
});

const getRestOfTitleValidateCases = [
  {
    parameter: "",
    expected: "",
  },
  {
    parameter: "test one two",
    expected: " one two",
  },
  {
    parameter: "test one",
    expected: " one",
  },
];

describe("Get rest of title.", () => {
  test.each(
    getRestOfTitleValidateCases.map((testCase) => [
      testCase.parameter,
      testCase.expected,
    ])
  )(
    `Test %#: Received the parameter: ${JSON.stringify(
      "%j"
    )}, expected corrected validation of ${JSON.stringify("%j")}.`,
    (parameter, expected) => {
      expect(expected).toStrictEqual(Utils.getRestOfTitle(parameter));
    }
  );
});

const getRemoveFirstCharacterOfStringValidateCases = [
  {
    parameter: "",
    expected: "",
  },
  {
    parameter: "test one two",
    expected: "est one two",
  },
  {
    parameter: " test one",
    expected: "test one",
  },
];

describe("Remove first character of string.", () => {
  test.each(
    getRemoveFirstCharacterOfStringValidateCases.map((testCase) => [
      testCase.parameter,
      testCase.expected,
    ])
  )(
    `Test %#: Received the parameter: ${JSON.stringify(
      "%j"
    )}, expected corrected validation of ${JSON.stringify("%j")}.`,
    (parameter, expected) => {
      expect(expected).toStrictEqual(
        Utils.removeFirstCharacterOfString(parameter)
      );
    }
  );
});

const firstItemImages: ISpotifyAlbumImagesInterface[] = [
  {
    height: 640,
    url: "https://i.scdn.co/image/ab67616d0000b2734042ca1eaf2d769794da7930",
    width: 640,
  },
  {
    height: 300,
    url: "https://i.scdn.co/image/ab67616d00001e024042ca1eaf2d769794da7930",
    width: 300,
  },
  {
    height: 64,
    url: "https://i.scdn.co/image/ab67616d000048514042ca1eaf2d769794da7930",
    width: 64,
  },
];

const secondItemImages: ISpotifyAlbumImagesInterface[] = [
  {
    height: 640,
    url: "https://i.scdn.co/image/ab67616d0000b2734042ca1eaf2d769794da7930",
    width: 640,
  },
  {
    height: 300,
    url: "https://i.scdn.co/image/ab67616d00001e024042ca1eaf2d769794da7930",
    width: 300,
  },
  {
    height: 64,
    url: "https://i.scdn.co/image/ab67616d000048514042ca1eaf2d769794da7930",
    width: 64,
  },
];

const getImagesByAlbumTitleValidateCasesParameterItem: ISpotifyAlbumResponseInterface =
  {
    href: "https://api.spotify.com/v1/artists/74ASZWbe4lXaubB36ztrGX/albums?offset=5&limit=50&include_groups=album,single,compilation,appears_on",
    items: [
      {
        album_group: "album",
        album_type: "album",
        href: "https://api.spotify.com/v1/albums/3ndKursZKjdkhOKSkwHDLW",
        id: "3ndKursZKjdkhOKSkwHDLW",
        images: firstItemImages,
        name: "Planet Waves",
        release_date: "2018-07-27",
        release_date_precision: "day",
        total_tracks: 29,
        type: "album",
        uri: "spotify:album:3ndKursZKjdkhOKSkwHDLW",
      },
      {
        album_group: "album",
        album_type: "album",
        href: "https://api.spotify.com/v1/albums/3JNLQ0r6jHvyJYeAuzPSBw",
        id: "3JNLQ0r6jHvyJYeAuzPSBw",
        images: secondItemImages,
        name: "Blood on the Tracks",
        release_date: "2018-07-18",
        release_date_precision: "day",
        total_tracks: 30,
        type: "album",
        uri: "spotify:album:3JNLQ0r6jHvyJYeAuzPSBw",
      },
    ],
    limit: 50,
    next: "https://api.spotify.com/v1/artists/74ASZWbe4lXaubB36ztrGX/albums?offset=55&limit=50&include_groups=album,single,compilation,appears_on",
    offset: 5,
    previous:
      "https://api.spotify.com/v1/artists/74ASZWbe4lXaubB36ztrGX/albums?offset=0&limit=50&include_groups=album,single,compilation,appears_on",
    total: 91,
  };

const firstParameter: string = "Planet Waves";
const secondParameter: string = "Blood on the Tracks";

const getImagesByAlbumTitleValidateCases = [
  {
    parameter: firstParameter,
    expected: [
      {
        height: 640,
        url: "https://i.scdn.co/image/ab67616d0000b2734042ca1eaf2d769794da7930",
        width: 640,
      },
      {
        height: 300,
        url: "https://i.scdn.co/image/ab67616d00001e024042ca1eaf2d769794da7930",
        width: 300,
      },
      {
        height: 64,
        url: "https://i.scdn.co/image/ab67616d000048514042ca1eaf2d769794da7930",
        width: 64,
      },
    ],
  },
  {
    parameter: secondParameter,
    expected: [
      {
        height: 640,
        url: "https://i.scdn.co/image/ab67616d0000b2734042ca1eaf2d769794da7930",
        width: 640,
      },
      {
        height: 300,
        url: "https://i.scdn.co/image/ab67616d00001e024042ca1eaf2d769794da7930",
        width: 300,
      },
      {
        height: 64,
        url: "https://i.scdn.co/image/ab67616d000048514042ca1eaf2d769794da7930",
        width: 64,
      },
    ],
  },
];

describe("Get images by album title.", () => {
  test.each(
    getImagesByAlbumTitleValidateCases.map((testCase) => [
      testCase.parameter,
      testCase.expected,
    ])
  )(
    `Test %#: Received the parameter: ${JSON.stringify(
      "%j"
    )}, expected corrected validation of ${JSON.stringify("%j")}.`,
    (parameter, expected) => {
      const response = Utils.getImagesByAlbumTitle(
        parameter as string,
        getImagesByAlbumTitleValidateCasesParameterItem
      );

      expect(response).toEqual(expected);
    }
  );
});

const dataArray = [
  {
    key: "data1",
    data: {
      subKey: 1,
    },
  },
  {
    key: "data1",
    data: {
      subKey: 2,
    },
  },
];

const groupByValidCases = [
  {
    parameter: dataArray,
    validKey: "key",
    expected: {
      data1: [
        { key: "data1", data: { subKey: 1 } },
        { key: "data1", data: { subKey: 2 } },
      ],
    },
  },
];

describe("Group array by.", () => {
  test.each(
    groupByValidCases.map((testCase) => [
      testCase.parameter,
      testCase.validKey,
      testCase.expected,
    ])
  )(
    `Test %#: Received the parameter: ${JSON.stringify(
      "%j"
    )}, expected corrected validation of ${JSON.stringify("%j")}.`,
    (parameter, validKey, expected) => {
      const response = Utils.groupBy(parameter as any[], validKey as string);

      expect(response).toEqual(expected);
    }
  );
});
