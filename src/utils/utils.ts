import { IAlbumData } from "interfaces/albumData.interface";
import { ISpotifyAlbumImagesInterface } from "interfaces/spotifyAlbumImages.interface";
import { ISpotifyAlbumResponseInterface } from "interfaces/spotifyAlbumResponse.interface";

export default class Utils {
  static reportError = ({ message }: { message: string }) => {
    console.log(message);
  };

  static getFirstWord = (title: string) => title.replace(/ .*/, "");

  static getRestOfTitle = (title: string) =>
    title.substring(title.indexOf(" "));

  static removeFirstCharacterOfString = (text: string) => text.substring(1);

  static getImagesByAlbumTitle = (
    albumTitle: string,
    artistAlbums: ISpotifyAlbumResponseInterface
  ): ISpotifyAlbumImagesInterface[] => {
    let data = artistAlbums.items.find(
      (album) => album.name.toLowerCase() === albumTitle.toLowerCase()
    );
    if (typeof data === "undefined") return [];
    return data.images;
  };

  static groupBy = (items: any[], key: string) => {
    return items.reduce(
      (
        result: { [x: string]: any },
        item: { [x: string]: string | number }
      ) => ({
        ...result,
        [item[key]]: [...(result[item[key]] || []), item],
      }),
      {}
    );
  };

  static getDataFromFile = (
    artistAlbums: ISpotifyAlbumResponseInterface,
    filePath: string
  ) => {
    let dataArray: IAlbumData[] = [];

    const fs = require("fs");

    try {
      let data = fs.readFileSync(filePath, "utf8");

      const arr = data.toString().replace(/\r\n/g, "\n").split("\n");

      arr.forEach((listItem: string) => {
        const albumTitle = Utils.removeFirstCharacterOfString(
          Utils.getRestOfTitle(listItem)
        );
        const albumYear = Utils.getFirstWord(listItem);
        const albumImages = this.getImagesByAlbumTitle(
          albumTitle,
          artistAlbums
        );
        dataArray.push({
          year: albumYear,
          title: albumTitle,
          cover: albumImages,
          trelloListTitle: `The 's${albumYear.charAt(2)}0s decade.`,
        });
      });

      const sortedArray = dataArray.sort((a, b) => (a.year > b.year ? 1 : -1));
      return this.groupBy(sortedArray, "trelloListTitle");
    } catch (error) {
      let message = "Unknown Error";
      if (error instanceof Error) message = error.message;
      Utils.reportError({ message });
    }
  };
}
