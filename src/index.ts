require("dotenv").config();
import express from "express";

import { ISpotifyAlbumResponseInterface } from "./interfaces/spotifyAlbumResponse.interface";

import Utils from "./utils/utils";
import TrelloHttp from "./http/trello.http";

const SpotifyWebApi = require("spotify-web-api-node");

const router = express.Router();

const app = express();

const spotifyApi = new SpotifyWebApi({
  clientId: process.env.SPOTIFY_CLIENT_ID,
  clientSecret: process.env.SPOTIFY_CLIENT_SECRET,
  redirectUri: process.env.SPOTIFY_REDIRECT_URL,
});

router.get("/login", (req, res, next) => {
  res.redirect(
    spotifyApi.createAuthorizeURL([
      "ugc-image-upload",
      "user-read-playback-state",
      "user-modify-playback-state",
      "user-read-currently-playing",
      "user-read-private",
      "user-read-email",
      "user-follow-modify",
      "user-follow-read",
      "user-library-modify",
      "user-library-read",
      "streaming",
      "app-remote-control",
      "user-read-playback-position",
      "user-top-read",
      "user-read-recently-played",
      "playlist-modify-private",
      "playlist-read-collaborative",
      "playlist-read-private",
      "playlist-modify-public",
    ])
  );
});

const getArtistAlbums = () => {
  spotifyApi
    .getArtistAlbums(process.env.SPOTIFY_ARTIST_KEY, { limit: 50, offset: 5 })
    .then(
      async (data: { body: ISpotifyAlbumResponseInterface }) => {
        const albumsList = Utils.getDataFromFile(
          data.body,
          "./public/discography.txt"
        );

        TrelloHttp.createBulkTrelloList(albumsList);
      },
      function (error: { message: string }) {
        let message = "Unknown Error";
        if (error instanceof Error) message = error.message;
        Utils.reportError({ message });
      }
    );
};

const getPersonalInfo = (accessToken: string) => {
  spotifyApi.getMe().then(
    (data: { body: any }) => {
      console.log(data.body);
    },
    function (error: { message: string }) {
      let message = "Unknown Error";
      if (error instanceof Error) message = error.message;
      Utils.reportError({ message });
    }
  );
};

router.get("/callback", (req, res, next) => {
  const code = req.query.code;
  spotifyApi.authorizationCodeGrant(code).then((response: any) => {
    let accessToken = response.body.access_token;
    spotifyApi.setAccessToken(accessToken);
    getPersonalInfo(accessToken);
    getArtistAlbums();
  });
});

app.use("/", router);

app.listen(8888, () => {
  console.log(`server running on port 8888`);
});
